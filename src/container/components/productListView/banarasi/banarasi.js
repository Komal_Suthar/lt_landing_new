import { Component } from "react";
import "./style.css";
import { List, Card, Tag, Button, Image, Space, Skeleton } from "antd";
import get from "lodash/get";
import find from "lodash/find";
import isEmpty from "lodash/isEmpty";
import remove from "lodash/remove";
import isUndefined from "lodash/isUndefined";
import { requestList } from "../../list/actions/list";
import { connect } from "react-redux";
import ShowMoreText from "react-show-more-text";
import { getTokenUser } from "../../../../core/utils/appUser";
import { addInCart } from "../../../components/shoppingCart/components/redux/Shopping/shopping-actions";

const _ = { get, remove, isEmpty, isUndefined, find };

const { Meta } = Card;
class MainBanarasi extends Component {
  state = { visible: false, placement: "left" };

  componentDidMount() {
    this.props.onGetAllList();
    console.log(
      "mnessage from product page component",
      this.props.onGetAllList()
    );
  }
  onAddToCart = (id, price) => {
    const myData = localStorage.getItem("token");
    const user = getTokenUser();

    if (myData) {
      window.location.reload();
      this.props.handleAddInCart({
        user_id: user.id,
        product_id: id,
        quantity: 1,
        price: price,
      });
    } else {
      alert("Please login !");
    }
  };

  showDrawer = () => {
    this.setState({
      visible: true,
    });
  };

  onClose = () => {
    this.setState({
      visible: false,
    });
  };

  render() {
    const data = Object.values(this.props.listData);
    const cartData = this.props.cartData;
    const bucket = _.get(cartData, "data.rows", []);
    return (
      <>
        <div className="filterBG">
          <p>29,914 Items Found</p>
        </div>
        <div className="mainDiv">
          <div className="alert-info">
            {/* <FilterDrawer /> */}
            <Button className="buttonHeader">
              <Space>
                <span style={{ fontWeight: 900, color: "#d63956" }}>
                  {data.length}
                </span>
                <span style={{ fontWeight: 600, color: "#d63956" }}>
                  Products Available !
                </span>
              </Space>
            </Button>
          </div>

          <div className="listHeader"></div>
          <Tag />
          <div className="containerDiv">
            <List
              pagination={{
                onChange: (page) => { },
                pageSize: 30,
              }}
              grid={{
                gutter: 20,
                xs: 1,
                sm: 1,
                md: 2,
                lg: 3,
                xl: 4,
                xxl: 5,
              }}
              dataSource={data}
              renderItem={(item) => (
                <List.Item align="middle">
                  <Card
                    hoverable
                    cover={
                      <Image
                        preview={false}
                        width={230}
                        height={250}
                        src={
                          `${process.env.REACT_APP_ASSETS_END_POINT}/products/${item.product_image}` || (
                            <Skeleton active />
                          )
                        }
                      />
                    }
                  >
                    <Meta
                      title={item.title}
                      description={
                        <>
                          <ShowMoreText
                            expanded={false}
                            more="Show more"
                            width={410}
                            less="Show less"
                            lines={1}
                          >
                            {item.description}
                          </ShowMoreText>
                        </>
                      }
                    />
                    <div style={{ fontWeight: 900, color: "green" }}>
                      <>₹{item.price}/- Only</>
                    </div>
                    <Button
                      onClick={() => this.onAddToCart(item.id, item.price)}
                      disabled={bucket.find(
                        (bkt) => bkt.product_id === item.id
                      )}
                      className="button-style"
                      type="primary"
                    >
                      {bucket.find((bkt) => bkt.product_id === item.id)
                        ? "Add In Bucket"
                        : "Add to cart"}
                    </Button>
                  </Card>
                </List.Item>
              )}
            />
          </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    listData: _.get(state, "list.listData", []),
    cartData: _.get(state, "shop.cartData", []),
    updateCart: _.get(state, "shop.updateCart", false),
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    onGetAllList: (payload) => dispatch(requestList(payload)),
    handleAddInCart: (payload) => dispatch(addInCart(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainBanarasi);
