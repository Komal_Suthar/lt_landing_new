import React from "react";
import { Button, Popover, Avatar, Upload, Space, Image } from "antd";
import "./style.css";
import MainOrderHistory from "../../components/orderHistory/orderHistory";
import {
  SettingOutlined,
  OrderedListOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { getTokenUser } from "../../../core/utils/appUser";
import historyToken from "../../../core/utils/history";
import defaultUser from "../../../assets/images/defaultUser.jpg";

const MainProfile = () => {
  const removeItem = () => {
    localStorage.removeItem("token");
    historyToken.push("/");
    window.location.reload();
  };
  const user = getTokenUser();
  const profile = `${process.env.REACT_APP_ASSETS_END_POINT}users/${user.profile_image} `;
  const content = (
    <div>
      <div style={{ textAlign: "center" }}>
        <div style={{ padding: 8, marginTop: -5 }}>
        </div>
        <div>
          <h2>
            {user.first_name} {user.last_name}
          </h2>
          <i>
            {user.email}
          </i>
        </div>
        <div style={{ display: "flex", marginTop: 5, justifyContent: 'center' }}>
          <Button onClick={removeItem} style={{ color: "white", backgroundColor: '#d8435f' }}>
            <LogoutOutlined />Logout
          </Button>
        </div>
      </div>
    </div>
  );
  return (
    <>
      <Popover trigger="click" content={content} placement="bottomRight">
        <Button style={{ fontSize: "2vh" }} type="text">
          {/* <Avatar src={profile ? profile : defaultUser} /> */}
          <b style={{ fontSize: 13 }}>
            {user.first_name} {""} {user.last_name}
          </b>
        </Button>
      </Popover>
    </>
  );
};
export default MainProfile;
