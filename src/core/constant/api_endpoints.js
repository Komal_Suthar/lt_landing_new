const API_ENDPOINTS = {
  login: "login",
  user: "user",
  banners: "banners",
  products: "products",
  categories: "categories",
  contact: "contact",
  register: "register",
  addtocart: "addtocart",
  order: "order",
  orderdetails: "orderdetails",
  contactUs: "contactUs",
};
export default API_ENDPOINTS;
